#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {

	if (argc != 2 && argc != 3) {
		printf("catl: usage: %s [gutter-width] <input-file>\n", argv[0]);
		return -1;
	}

	if (strcmp(argv[1], "-h") == 0) {
		printf("catl\n\nDump a text file to the console with line numbering.\n");
		printf("Usage: %s [gutter-width] <input-file>\n", argv[0]);
		return -1;
	}

	// Default gutter width is 4 spaces wide
	int gutter_width = 4;

	char gutter_format[5];
	if (argc == 3) {
		gutter_width = (int)strtol(argv[1], NULL, 10);
		if (gutter_width < 1 || gutter_width > 9) {
			printf("catl: [error]\tgutter-width must be in the range [1-9]\n");
			return -1;
		}
	}
	sprintf(gutter_format, "%%-%dd", gutter_width);

	FILE *inputfile = (argc >= 3) ? fopen(argv[2], "r") : fopen(argv[1], "r");
	if (inputfile) {
		int line = 1;
		char c = NULL;
		int rc = fread(&c, sizeof(char), 1, inputfile);
		int newline = 1;
		while (rc) {
			if (newline) {
				printf(gutter_format, line);
				newline = 0;
			}
			printf("%c", c);
			if (c == '\n') {
				line++;
				newline = 1;
			}
			rc = fread(&c, sizeof(char), 1, inputfile);
		}
		if (ferror(inputfile)) {
			printf("catl: error: The input file was not read completely.\n");
		}
		fclose(inputfile);
	} else {
		printf("catl: error: The specified file could not be opened for reading.\n");
	}
	return 0;
}
