#catl

This program writes the contents of a file specified as argument to stdout, with
the line numbers of the lines in the file.

### Usage
```
catl [line-number-width] file
```

### Example usage
```
catl somefile
```

The command above will print the contents of somefile to stdout. In this command
no width was specified for the line numbers so a default of 4 is used.

The command below specifies a width (8 spaces) for the line numbers.

```
catl 8 somefile
```
